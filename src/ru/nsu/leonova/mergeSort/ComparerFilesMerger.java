package ru.nsu.leonova.mergeSort;

import ru.nsu.leonova.compare.CompareStrategy;
import ru.nsu.leonova.compare.ParseException;

import java.io.*;

public class ComparerFilesMerger<T> implements TwoFilesMerger {

    private CompareStrategy<T> compareStrategy;

    public ComparerFilesMerger(CompareStrategy<T> compareStrategy){
        this.compareStrategy = compareStrategy;
    }

    public void sort(BufferedReader file1, BufferedReader file2, BufferedWriter result){

        T value1 = getNextValidValue(file1, null);
        T value2 = getNextValidValue(file2, null);


        try {
            while(value1 != null && value2 != null){

                if(compareStrategy.compare(value1, value2) < 0){
                    result.write(value1.toString()+"\n");
                    value1 = getNextValidValue(file1, value1);
                }
                else{
                    result.write(value2.toString()+"\n");
                    value2 = getNextValidValue(file2, value2);
                }

            }

            flushFile(file1, result, value1);
            flushFile(file2, result, value2);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private T getNextValidValue(BufferedReader file, T oldValue){

        for(;;){
            try {
                String line = file.readLine();
                if(line == null){
                    return null;
                }
                T retValue = compareStrategy.prepareArgument(line);
                if(oldValue != null && compareStrategy.compare(oldValue, retValue) > 0){
                    System.err.println("Ignore second value " + oldValue + " " + retValue);
                    continue;
                }
                return retValue;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ParseException e){
                System.out.println("Couldn't parse");
            }
        }
    }

    private void flushFile(BufferedReader fileIn, BufferedWriter fileOut, T oldValue) throws IOException{

        if(oldValue == null){
            return;
        }
        else{
            fileOut.write(oldValue + "\n");
        }

        T line;
        while((line = getNextValidValue(fileIn, oldValue)) != null){
            fileOut.write(line.toString() + "\n");

        }
    }
}
