package ru.nsu.leonova.mergeSort;

import java.io.*;
import java.util.Queue;

public class MergerAllFiles{

    private TwoFilesMerger mergerTwoFiles;

    public MergerAllFiles(TwoFilesMerger filesMerger){
        mergerTwoFiles = filesMerger;
    }

    public void sort(Queue<String> files, String out){

        while(files.size() > 1){
            String fileName1 = files.remove();
            String fileName2 = files.remove();

            try(BufferedReader file1 = new BufferedReader(new FileReader(fileName1));
                BufferedReader file2 = new BufferedReader(new FileReader(fileName2));
                BufferedWriter fileOut = new BufferedWriter(new FileWriter(createOut(files, out)))){

                mergerTwoFiles.sort(file1, file2, fileOut);
            }
            catch (IOException e){
                System.err.println("Unable to merge files " + fileName1 + " " + fileName2 + ":\n" + e.getMessage());
            }
        }
    }

    private File createOut(Queue<String> files, String out) throws IOException {
        if(files.size() != 0){
            File temp = File.createTempFile("merge", "tmp");
            temp.deleteOnExit();
            files.add(temp.getAbsolutePath());
            return temp;
        }
        return new File(out);
    }

}
