package ru.nsu.leonova.mergeSort;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public interface TwoFilesMerger {

    void sort(BufferedReader file1, BufferedReader file2, BufferedWriter result);
}
