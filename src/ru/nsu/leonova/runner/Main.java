package ru.nsu.leonova.runner;

import ru.nsu.leonova.compare.IntegerStrategy;
import ru.nsu.leonova.compare.StringStrategy;
import ru.nsu.leonova.mergeSort.ComparerFilesMerger;
import ru.nsu.leonova.mergeSort.MergerAllFiles;
import ru.nsu.leonova.mergeSort.TwoFilesMerger;

import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        Boolean inverseSort = null;
        SortType type = SortType.UNKNOWN;

        if(args.length == 0){
            System.err.println("No parameters");
            return;
        }

        int i = 0;
        while(args[i].charAt(0) == '-' && i < args.length){

            String param = args[i++].substring(1);

            switch (param){
                case "d":
                    if(inverseSort == null){
                        inverseSort = true;
                        continue;
                    }
                    System.err.println("Duplicate sort parameter");
                    return;
                case "a":
                    if(inverseSort == null){
                        inverseSort = false;
                        continue;
                    }
                    System.err.println("Duplicate sort parameter");
                    return;
                case "i":
                    if(type == SortType.UNKNOWN){
                        type = SortType.INTEGER;
                        continue;
                    }
                    System.err.println("Duplicate type parameter");
                    return;
                case "s":
                    if(type == SortType.UNKNOWN){
                        type = SortType.STRING;
                        continue;
                    }
                    System.err.println("Duplicate type parameter");
                    return;
                default :
                    System.err.println("Unknown parameter " + param);
                    return;

            }

        }
        if(inverseSort == null){
            inverseSort = false;
        }
        if(type == SortType.UNKNOWN){
            System.err.println("You must specify -s or -i");
            return;
        }
        
        if(args.length <= i + 2){
            System.err.println("You should specify files: output and at least 2 input");
        }
        
        String fileOut = args[i++];

        Queue<String> filesIn = new LinkedList<>();        
        while(i < args.length){
            filesIn.add(args[i++]);
        }


        TwoFilesMerger twoFilesMerger = null;
        switch (type){
            case STRING:
                twoFilesMerger = new ComparerFilesMerger<>(new StringStrategy(inverseSort));
                break;
            case INTEGER:
                twoFilesMerger = new ComparerFilesMerger<>(new IntegerStrategy(inverseSort));
                break;
        }

        MergerAllFiles merger = new MergerAllFiles(twoFilesMerger);
        merger.sort(filesIn, fileOut);
    }


}
