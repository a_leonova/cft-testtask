package ru.nsu.leonova.compare;

public interface CompareStrategy <T>{

    T prepareArgument(String s) throws ParseException;
    int compare(T first, T second);
}
