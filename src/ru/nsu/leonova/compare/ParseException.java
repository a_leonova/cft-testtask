package ru.nsu.leonova.compare;

public class ParseException extends Exception {
    public ParseException(String message){
        super(message);
    }
}
