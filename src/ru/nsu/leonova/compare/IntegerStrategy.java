package ru.nsu.leonova.compare;

public class IntegerStrategy implements CompareStrategy<Integer>{

    private int sign;

    public IntegerStrategy(boolean inverseSort) {
        sign = inverseSort ? -1 : 1;
    }

    @Override
    public Integer prepareArgument(String s) throws ParseException {
       try{
           return Integer.parseInt(s);
       } catch(NumberFormatException e){
           throw new ParseException(e.getMessage());
       }

    }

    @Override
    public int compare(Integer first, Integer second) {
        return first.compareTo(second) * sign;
    }
}
