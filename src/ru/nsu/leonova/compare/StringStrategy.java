package ru.nsu.leonova.compare;

public class StringStrategy implements CompareStrategy<String> {

    private int sign;

    public StringStrategy(boolean inverseSort) {
        sign = inverseSort ? -1 : 1;
    }

    @Override
    public String prepareArgument(String s) {
        return s;
    }

    @Override
    public int compare(String first, String second) {
        return first.compareTo(second) * sign;
    }
}
